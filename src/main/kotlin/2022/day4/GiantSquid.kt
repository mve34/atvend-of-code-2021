package day4

import getPuzzleInput


fun day4Part1() {
    val inputString = getPuzzleInput(4).toMutableList()
    val bingoOrder = inputString[0].split(",").toTypedArray()
    val bingoLists = createBingoLists(inputString)

    for (bingoNumber in bingoOrder) {
        bingoLists.forEach { bingoArray ->
            for (i in bingoArray.indices) {
                for (j in bingoArray[i].indices) {
                    if (bingoArray[i][j]?.getNumber() == bingoNumber) {
                        bingoArray[i][j]?.mark()
                    }
                }
                if (checkCurrentListForBingo(bingoArray)) {
                    println(getSum(bingoArray) * bingoNumber.toInt())
                    return
                }
            }
        }
    }
}

fun day4Part2() {
    val inputString = getPuzzleInput(4).toMutableList()
    val bingoOrder = inputString[0].split(",").toTypedArray()
    val bingoLists = createBingoLists(inputString)
    val skipLists: Array<Boolean> = Array(bingoLists.size) { false }
    var counter = 0

    for (number in bingoOrder.indices) {
        for (index in bingoLists.indices) {
            if (!skipLists[index]) {
                for (i in bingoLists[index].indices) {
                    for (j in bingoLists[index][i].indices) {
                        if (bingoLists[index][i][j]?.getNumber() == bingoOrder[number]) {
                            bingoLists[index][i][j]?.mark()
                        }
                    }
                    if (checkCurrentListForBingo(bingoLists[index]) && !skipLists[index]) {
                        counter++
                        skipLists[index] = true
                        if (counter == bingoLists.size) {
                            println(getSum(bingoLists[index]) * bingoOrder[number].toInt())
                            return
                        }
                    }
                }
            }
        }
    }
}

fun checkCurrentListForBingo(bingoList: Array<Array<Bingo?>>): Boolean {
    for (i in bingoList.indices) {
        if (bingoList[i].all { it?.isMarked() == true }) return true
        if (bingoList.all { it[i]?.isMarked() == true }) return true
    }
    return false
}

fun getSum(bingoCard: Array<Array<Bingo?>>): Int {
    var sum = 0
    for (i in bingoCard.indices) {
        for (j in bingoCard[0].indices) {
            if (!bingoCard[i][j]?.isMarked()!!) sum += bingoCard[i][j]?.getNumber()?.toInt()!!
        }
    }
    return sum
}

fun createBingoLists(inputString: MutableList<String>): (MutableList<Array<Array<Bingo?>>>) {
    val input: MutableList<Array<Array<Bingo?>>> = ArrayList()
    var bingoPuzzle = Array(5) { arrayOfNulls<Bingo>(5) }

    var s: String
    var line: Array<String>

    val iteratedInput = inputString.listIterator()
    iteratedInput.next()

    while (iteratedInput.hasNext()) {
        if (iteratedInput.next() == "") continue
        iteratedInput.previous()
        for (j in bingoPuzzle.indices) {
            s = iteratedInput.next().trim { it == ' ' }
            line = s.split("\\s+".toRegex()).toTypedArray()
            for (k in line.indices) {
                bingoPuzzle[j][k] = Bingo(line[k])
            }
        }
        input.add(bingoPuzzle)
        bingoPuzzle = Array(5) { arrayOfNulls(5) }
    }
    return input
}

class Bingo internal constructor(private var number: String) {
    private var marked = false
    fun isMarked(): Boolean {
        return marked
    }

    fun mark(): Boolean {
        return true.also { marked = it }
    }

    fun getNumber(): String {
        return this.number
    }
}
