package day6

import getPuzzleInput

fun day6Part1() {
    val inputString = getPuzzleInput(6)
    val numberOfDays = 80

    val ageArray: MutableList<Int> = inputString[0].split(",").map { it.toInt() }.toMutableList()

    repeat(numberOfDays) {
        var toAdd = 0
        ageArray.forEachIndexed { i, age ->
            if (age == 0) {
                ageArray[i] = 6
                toAdd++
            } else {
                ageArray[i]--
            }
        }
        repeat(toAdd) {
            ageArray.add(8)
        }
    }
    println(ageArray.count())
}

fun day6Part2() {
    val inputString = getPuzzleInput(6)
    val numberOfDays = 256
    var totalFish: Long = 0

    var ageMap: MutableMap<Int, Long> =
        mutableMapOf(0 to 0, 1 to 0, 2 to 0, 3 to 0, 4 to 0, 5 to 0, 6 to 0, 7 to 0, 8 to 0)

    inputString[0].split(",").forEach {
        ageMap[it.toInt()] = ageMap[it.toInt()]!! + 1
    }
    ageMap = ageMap.toSortedMap()

    repeat(numberOfDays) {
        var newFishAmount: Long = 0
        for ((age, number) in ageMap) {
            if (number != 0L) {
                if (age == 0) {
                    newFishAmount = number
                } else {
                    ageMap.putIfAbsent(age - 1, 0)
                    ageMap[age - 1] = ageMap[age - 1]!! + number
                }
                ageMap[age] = 0
            }
        }
        ageMap[8] = ageMap[8]?.plus(newFishAmount)
        ageMap[6] = ageMap[6]?.plus(newFishAmount)
    }
    ageMap.mapValues { (_, number) -> totalFish += number }
    println(totalFish)
}
