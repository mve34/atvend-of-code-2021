package day5

import getPuzzleInput
import java.awt.geom.Line2D

fun day5Part1() {
    val inputString = getPuzzleInput(5)
    val lineArray: ArrayList<Line2D> = arrayListOf()
    val arrayInput: ArrayList<ArrayList<String>> = arrayListOf()
    var count = 0

    for (i in inputString.indices) {
        arrayInput.add(
            arrayListOf(inputString[i].split(" -> ")[0], inputString[i].split(" -> ")[1])
        )
    }

    arrayInput.forEach {
        if (it[0].split(",")[0] != it[1].split(",")[0] && it[0].split(",")[1] != it[1].split(",")[1]) {
            return@forEach
        }
        val line: Line2D = Line2D.Double()
        line.setLine(
            it[0].split(",")[0].toDouble(),
            it[0].split(",")[1].toDouble(),
            it[1].split(",")[0].toDouble(),
            it[1].split(",")[1].toDouble()
        )
        lineArray.add(line)
    }

    stop@ for (i in lineArray.indices) {
        for (j in lineArray.indices) {
            if (j <= i) continue
            if (lineArray[i].intersectsLine(lineArray[j])) {
                count++
            }
        }
    }
    println(count)
}

fun day5Part2() {
    val inputString = getPuzzleInput(5)

}
