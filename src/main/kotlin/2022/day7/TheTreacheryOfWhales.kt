package day7

import getPuzzleInput
import kotlin.math.min
import kotlin.math.roundToInt

fun day7Part1() {
    val inputString = getPuzzleInput(7)[0].split(",").map { it.toInt() }.sorted()

    val bestPosition = inputString[inputString.size / 2]
    var totalFuel = 0

    inputString.forEach { distance ->
        if (distance == bestPosition) return@forEach
        else if (distance > bestPosition) {
            totalFuel += distance - bestPosition
        } else if (distance < bestPosition) {
            totalFuel += bestPosition - distance
        }
    }
    println(totalFuel)
}

fun day7Part2() {
    val inputString = getPuzzleInput(7)[0].split(",").map { it.toInt() }

    val bestPositionFloor = inputString.toIntArray().average().toInt()
    val bestPositionCeil = inputString.toIntArray().average().roundToInt()

    println(
        if (bestPositionCeil != bestPositionFloor) {
            min(inputString.stuff(bestPositionCeil), inputString.stuff(bestPositionFloor))
        } else inputString.stuff(bestPositionFloor)
    )
}

fun List<Int>.stuff(bestPosition: Int): Int {
    var totalFuel = 0
    this.forEach { distance ->
        if (distance == bestPosition) return@forEach
        else if (distance > bestPosition) {
            totalFuel += (distance - bestPosition) * (distance - bestPosition + 1) / 2
        } else if (distance < bestPosition) {
            totalFuel += (bestPosition - distance) * (bestPosition - distance + 1) / 2
        }
    }
    return totalFuel
}
