package day3

import getPuzzleInput

fun day3Part1() {
    val inputString = getPuzzleInput(3)

    val array: Array<CharArray> = reverseArray(inputString)
    var gammaRateString = ""
    var eliptionRateString = ""

    array.forEach {
        gammaRateString += it.sorted()[it.size / 2]
        eliptionRateString += (if (it.sorted()[it.size / 2].toString() == "1") "0" else "1")
    }

    println(Integer.parseInt(gammaRateString, 2) * Integer.parseInt(eliptionRateString, 2))
}

fun day3Part2() {
    println(forLoopPart2(false) * forLoopPart2(true))
}

fun reverseArray(inputString: List<String>): Array<CharArray> {
    val array: Array<CharArray> = Array(inputString[0].length) { CharArray(inputString.size) }
    for (i in inputString.indices) {
        for (j in inputString[i].indices) {
            array[j][i] = inputString[i][j]
        }
    }
    return array
}

fun forLoopPart2(oxygen: Boolean): Int {
    val inputString = getPuzzleInput(3).toMutableList()

    val ones = IntArray(inputString[0].length)
    val zeros = IntArray(inputString[0].length)

    for (i in reverseArray(inputString).indices) {
        val array = reverseArray(inputString)
        for (j in array[i].indices) if (array[i][j] == '1') ones[i]++ else zeros[i]++

        if (ones[i] < zeros[i]) {
            var j = inputString.size - 1
            while (j >= 0) {
                if (inputString[j].toCharArray()[i] == if (oxygen) '1' else '0') {
                    inputString.removeAt(j)
                }
                j--
            }
        } else {
            var j = inputString.size - 1
            while (j >= 0) {
                if (inputString[j].toCharArray()[i] == if (oxygen) '0' else '1') {
                    inputString.removeAt(j)
                }
                j--
            }
        }
        if (inputString.size == 1) break
    }
    return Integer.parseInt(inputString[0], 2)
}
