package day2

import getPuzzleInput

fun day2Part1() {
    val inputString = getPuzzleInput(2)

    val usersCount: Map<String, Int> = inputString
        .groupBy { it.split(" ")[0] }
        .mapValues { (_, distance) -> distance.sumOf { it.split(" ")[1].toInt() } }
    println(usersCount["forward"]!!.times((usersCount["down"]!!.minus(usersCount["up"]!!))))

}

fun day2Part2() {
    val inputString = getPuzzleInput(2)

    var aim = 0
    var depth = 0
    var distance = 0

    inputString.forEach {
        val direction = it.split(" ")[0]
        val value = it.split(" ")[1].toInt()
        when (direction) {
            "up" -> aim -= value
            "down" -> aim += value
            "forward" -> {
                distance += value
                depth += aim * value
            }
        }
    }

    println(depth.times(distance))
}

