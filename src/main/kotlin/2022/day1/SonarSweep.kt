package day1

import getPuzzleInput

fun day1Part1() {
    val inputString = getPuzzleInput(1)
    var counter = 0

    for (i in inputString.indices) {
        if (i == 0) {
            continue
        } else {
            try {
                if (inputString[i] > inputString[i - 1]) {
                    counter++
                }
            } catch (e: Exception) {
                println(e)
            }
        }
    }
    println(counter)
}

fun day1Part2() {
    val inputString = getPuzzleInput(1)
    var counter = 0

    try {
        for (i in inputString.indices) {
            val firstMeasurement = inputString[i].toInt() + inputString[i + 1].toInt() + inputString[i + 2].toInt()
            val secondMeasurement = inputString[i + 1].toInt() + inputString[i + 2].toInt() + inputString[i + 3].toInt()
            if (firstMeasurement < secondMeasurement) {
                counter++
            }
        }
    } catch (e: Exception) {
    }
    println(counter)
}
