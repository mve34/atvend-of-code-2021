package day8

import getPuzzleInput

fun day8Part1() {
    val inputRow = getPuzzleInput(8)
    var count = 0
    val dataArray = splitInputIntoArray(inputRow)

    dataArray.forEach {
        it[1].forEach {
            when (it.count()) {
                2 -> count++
                3 -> count++
                4 -> count++
                7 -> count++
            }
        }
    }
    println(count)
}

fun day8Part2() {
    val inputRow = getPuzzleInput(8)
    val numberArray = arrayOfNulls<String>(inputRow.size)
    val dataArray = splitInputIntoArray(inputRow)
    var total = 0
    var four = ""

    dataArray.forEachIndexed { index, it ->
        val positions: MutableMap<String, String> = mutableMapOf("top" to "", "topLeftCenter" to "", "right" to "")
        var currentNumber = ""

        it[0].forEach { number ->
            when (number.length) {
                2 -> positions["right"] = number
                4 -> four = number
            }
        }
        four.forEach {
            if (!positions["right"]!!.contains(it)) {
                positions["topLeftCenter"] += it.toString()
            }
        }
        it[1].forEach { number ->
            var charactersOnTheRight = ""
            var charactersInTopLeft = ""

            positions["right"]!!.forEach {
                if (number.contains(it)) charactersOnTheRight += it.toString()
            }
            positions["topLeftCenter"]!!.forEach {
                if (number.contains(it)) charactersInTopLeft += it.toString()
            }
            currentNumber += when (number.length) {
                2 -> "1"
                3 -> "7"
                4 -> "4"
                7 -> "8"
                5 -> {
                    if (charactersOnTheRight.length == 1 && charactersInTopLeft.length == 1) {
                        "2"
                    } else {
                        if (charactersOnTheRight.length == 2) {
                            "3"
                        } else {
                            "5"
                        }
                    }
                }
                6 -> {
                    if (charactersOnTheRight.length == 2 && charactersInTopLeft.length == 1) {
                        "0"
                    } else {
                        if (charactersInTopLeft.length == 2 && charactersOnTheRight.length == 1) {
                            "6"
                        } else {
                            "9"
                        }
                    }
                }
                else -> ""
            }
        }
        numberArray[index] = currentNumber
    }
    numberArray.forEach { number ->
        total += number?.toInt() ?: 0
    }
    println(total)
}

fun splitInputIntoArray(inputRow: List<String>): Array<Array<Array<String>>> {
    var dataArray = arrayOf<Array<Array<String>>>()

    inputRow.forEach { line ->
        var parts = arrayOf<Array<String>>()
        line.split(" | ").forEach { part ->
            var elements = arrayOf<String>()
            part.split(" ").forEach { element ->
                elements += element
            }
            parts += elements
        }
        dataArray += parts
    }
    return dataArray
}
