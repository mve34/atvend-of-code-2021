import day1.day1Part1
import day1.day1Part2
import day2.day2Part1
import day2.day2Part2
import day3.day3Part1
import day3.day3Part2
import day4.day4Part1
import day4.day4Part2
import day5.day5Part1
import day5.day5Part2
import day6.day6Part1
import day6.day6Part2
import day7.day7Part1
import day7.day7Part2
import day8.day8Part1
import day8.day8Part2
import java.io.File

fun getPuzzleInput(day: Int): List<String> {
    val puzzleInput =
        File("F:\\IdeaProjects\\AdventOfCode\\src\\main\\resources\\puzzleInputDay$day.txt").bufferedReader()
    return puzzleInput.readLines()
}

fun main() {

    println("Day1:")
    print("Part1: ")
    day1Part1()
    print("Part2: ")
    day1Part2()

    println("\nDay2:")
    print("Part1: ")
    day2Part1()
    print("Part2: ")
    day2Part2()

    println("\nDay3:")
    print("Part1: ")
    day3Part1()
    print("Part2: ")
    day3Part2()

    println("\nDay4:")
    print("Part1: ")
    day4Part1()
    print("Part2: ")
    day4Part2()

    println("\nDay5:")
    print("Part1: ")
    day5Part1()
    print("Part2: ")
    day5Part2()

    println("\nDay6:")
    print("Part1: ")
    day6Part1()
    print("Part2: ")
    day6Part2()

    println("\nDay7:")
    print("Part1: ")
    day7Part1()
    print("Part2: ")
    day7Part2()

    println("\nDay8:")
    print("Part1: ")
    day8Part1()
    print("Part2: ")
    day8Part2()

}
